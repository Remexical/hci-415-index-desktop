﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class inputPassthrough : MonoBehaviour
{
    #if UNITY_EDITOR_WIN
    [DllImport("user32.dll")]
    static extern bool SetCursorPos(int x, int y);
    #endif
    #if UNITY_EDITOR_LINUX
    [DllImport("X11")]

    #endif
    private GameObject screen;
    public GameObject vrControllerLeft;
    public GameObject vrControllerRight;

    private Vector3 _raycastDirection;
    private float _oldMouseXPos;
    private float _oldMouseYPos;


    // Start is called before the first frame update
    void Start()
    {
        screen = this.gameObject;
    }

    public void RaycastUV()
    {

        //Raycast from the controller 
        Vector3 _origin = vrControllerRight.transform.position;
        Vector3 _direction = -vrControllerRight.transform.up;
        RaycastHit _uvRay = new RaycastHit();
        Physics.Raycast(_origin, _direction, out _uvRay);
        Debug.DrawRay(_origin, _direction);



        //Smooth mouse input to prevent extreme jitter
        float _smoothTime = 0.05f;
        float _smoothVeloc = 0.1f; //This likely needs to be seperated per axis
        float _mouseXPos = Mathf.SmoothDamp(_oldMouseXPos, _uvRay.textureCoord.x, ref _smoothVeloc, _smoothTime);
        float _mouseYPos = Mathf.SmoothDamp(_oldMouseYPos, _uvRay.textureCoord.y, ref _smoothVeloc, _smoothTime);

        //Cache old pos for smoothing
        _oldMouseXPos = _mouseXPos;
        _oldMouseYPos = _mouseYPos;



        //Set cursot position on the main screen
        int _screenWidth = Screen.currentResolution.width;
        int _screenHeight = Screen.currentResolution.height;
        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
        {
            SetCursorPos((int)(_mouseXPos*_screenWidth), _screenHeight - (int)(_mouseYPos*_screenHeight));
        } else
        {

        }

    }

    //private Vector3 
}
