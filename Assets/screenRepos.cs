﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class screenRepos : MonoBehaviour
{
    private GameObject screen;
    public GameObject vrControllerLeft;
    public GameObject vrControllerRight;


    private bool bIsLeftControllerPosLocked = false;
    private bool bIsRightControllerPosLocked = false;
    private bool bIsPosLocked = false;
    private Vector3 lockedVrControllerLeftPosition;
    private Vector3 lockedVrControllerRightPosition;
    //public GameObject 

    // Start is called before the first frame update
    void Start()
    {
        screen = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
    }

   
    public void updateScreenPosition(string side = "")
    {

        if (!bIsPosLocked)
        {

            

            if (bIsLeftControllerPosLocked == false && bIsRightControllerPosLocked == false)
            {
                
                ///Find average forward of hand to generate screen normal 
                Vector3 _avgForward = -vrControllerLeft.transform.right.normalized - -vrControllerRight.transform.right.normalized;

                ///Calculate the diagonal of the screen to find dimensions
                Vector3 _screenDiagonal = Vector3.ProjectOnPlane((vrControllerRight.transform.position - vrControllerLeft.transform.position), _avgForward);
                Debug.DrawRay(vrControllerLeft.transform.position, _screenDiagonal);

                ///Scale screen to diagonal dimensions
                Vector3 _avgPos = (vrControllerLeft.transform.position + vrControllerRight.transform.position) / 2f;
                screen.transform.position = _avgPos;
                setScreenRotation();
                screen.transform.localScale = new Vector3(Vector3.Project(_screenDiagonal, screen.transform.right).magnitude + 0.1f,
                                                          Vector3.Project(_screenDiagonal, screen.transform.up).magnitude + 0.1f, 1);


            }
            else if ((bIsLeftControllerPosLocked == true) && (bIsRightControllerPosLocked == false))
            {
                ///Find average forward of hand to get screen normal 
                Vector3 _screenDiagonal = Vector3.ProjectOnPlane((vrControllerRight.transform.position - lockedVrControllerLeftPosition), -this.transform.forward);
                Debug.DrawRay(vrControllerLeft.transform.position, _screenDiagonal, Color.green);

                ///Find middle of controller position
                Vector3 _avgPos = (lockedVrControllerLeftPosition + vrControllerRight.transform.position) / 2f;
                screen.transform.position = _avgPos;

                ///Scale screen to fit hand gesture dims
                screen.transform.localScale = new Vector3(Vector3.Project(_screenDiagonal, screen.transform.right).magnitude + 0.1f,
                                                         Vector3.Project(_screenDiagonal, screen.transform.up).magnitude + 0.1f, 1);



            }
            else if ((bIsLeftControllerPosLocked == false) && (bIsRightControllerPosLocked == true))
            {
                ///Calculate the diagonal of the screen to find dimensions
                Vector3 _screenDiagonal = Vector3.ProjectOnPlane((lockedVrControllerRightPosition - vrControllerLeft.transform.position), -this.transform.forward);
                Debug.DrawRay(vrControllerLeft.transform.position, _screenDiagonal, Color.blue);

                Vector3 _avgPos = (vrControllerLeft.transform.position + lockedVrControllerRightPosition) / 2f;
                screen.transform.position = _avgPos;

                ///Scale screen to fit hand gesture dims
                screen.transform.localScale = new Vector3(Vector3.Project(_screenDiagonal, screen.transform.right).magnitude + 0.1f,
                                                         Vector3.Project(_screenDiagonal, screen.transform.up).magnitude + 0.1f, 1);

            }
        }

        return;
    }

    private void setScreenRotation()///If both controllers are unlocked and resizing, rotate the screen to an averaged forward
    {
        Vector3 _avgForward = -vrControllerLeft.transform.right.normalized - -vrControllerRight.transform.right.normalized;
        Debug.DrawLine(screen.transform.position, screen.transform.position + _avgForward.normalized);

        screen.transform.LookAt(this.transform.position - _avgForward.normalized);
    }

    public void lockScreenCorner(string side) ///Lock screen corner when grab grip snaps
    {

        //When a controller lock chord is received, lock that corner of the screen. This allows for screens larger than single transverse
        switch (side)
        {
            case "L":
                bIsLeftControllerPosLocked = true;
                lockedVrControllerLeftPosition = vrControllerLeft.transform.position;
                break;
            case "R":
                bIsRightControllerPosLocked = true;
                lockedVrControllerRightPosition = vrControllerRight.transform.position;
                break;
            default:
                break;
        }
        return;
    }

    public void unlockScreen()
    {
        //Debug.Log("Unlocked");
        bIsLeftControllerPosLocked = false;
        bIsRightControllerPosLocked = false;

        //Likely unnecessary
        lockedVrControllerLeftPosition = Vector3.zero;
        lockedVrControllerRightPosition = Vector3.zero;
    }
}
