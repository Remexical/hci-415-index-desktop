//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Valve.VR
{
    using System;
    using UnityEngine;
    
    
    public class SteamVR_Input_ActionSet_default : Valve.VR.SteamVR_ActionSet
    {
        
        public virtual SteamVR_Action_Pose Pose
        {
            get
            {
                return SteamVR_Actions.default_Pose;
            }
        }
        
        public virtual SteamVR_Action_Skeleton SkeletonLeftHand
        {
            get
            {
                return SteamVR_Actions.default_SkeletonLeftHand;
            }
        }
        
        public virtual SteamVR_Action_Skeleton SkeletonRightHand
        {
            get
            {
                return SteamVR_Actions.default_SkeletonRightHand;
            }
        }
        
        public virtual SteamVR_Action_Single Squeeze
        {
            get
            {
                return SteamVR_Actions.default_Squeeze;
            }
        }
        
        public virtual SteamVR_Action_Boolean HeadsetOnHead
        {
            get
            {
                return SteamVR_Actions.default_HeadsetOnHead;
            }
        }
        
        public virtual SteamVR_Action_Boolean adjustScreen
        {
            get
            {
                return SteamVR_Actions.default_adjustScreen;
            }
        }
        
        public virtual SteamVR_Action_Boolean lockScreen
        {
            get
            {
                return SteamVR_Actions.default_lockScreen;
            }
        }
        
        public virtual SteamVR_Action_Boolean unlockScreen
        {
            get
            {
                return SteamVR_Actions.default_unlockScreen;
            }
        }
        
        public virtual SteamVR_Action_Boolean LeftClickDown
        {
            get
            {
                return SteamVR_Actions.default_LeftClickDown;
            }
        }
        
        public virtual SteamVR_Action_Vibration Haptic
        {
            get
            {
                return SteamVR_Actions.default_Haptic;
            }
        }
    }
}
